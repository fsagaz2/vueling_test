import { Injectable } from '@angular/core';
import {Http,HttpModule,JsonpModule, Jsonp, Response} from '@angular/http';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
private  API_URL  =  'https://stats.nba.com/stats';
  constructor(private jsonp: Jsonp) {
        this.jsonp = jsonp;
  }

  getPlayers(): Observable<Array<any>> {
    return this.jsonp.get<Array<any>>(`${this.API_URL}/leaguedashplayerbiostats/?PerMode=Totals&LeagueID=00&Season=2016-17&SeasonType=Regular Season&callback=JSONP_CALLBACK`)
     .pipe(map(res => res.json());
  }

  getPlayer(id: string) {
    return this.jsonp.get<any>(`${this.API_URL}/commonplayerinfo/?PlayerID=${id}&callback=JSONP_CALLBACK`)
      .pipe(map(res => res.json());
  }

}
