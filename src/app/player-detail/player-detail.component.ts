import { Component, OnInit } from '@angular/core';
import { ApiService } from  '../api.service';
import { Observable, of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
@Component({
    selector:  'app-player-detail',
    templateUrl:  './player-detail.component.html',
    styleUrls: ['./player-detail.component.css']
})

export  class  PlayerDetailComponent  implements  OnInit {

public playerInfo;

constructor(
  private  apiService:  ApiService,
  private activatedRoute: ActivatedRoute,
) { }

ngOnInit() {
    this.getPlayer(this.activatedRoute.snapshot.params['PLAYER_ID']);
}

public getPlayer(id:string){
  this.apiService.getPlayer(id).subscribe((player) => {
    const headers: Array<any> = player.resultSets[0].headers;
    const playerFirstNameIndex = headers.indexOf('FIRST_NAME');
    const playerLastNameIndex = headers.indexOf('LAST_NAME');
    const playerBirthDateIndex = headers.indexOf('BIRTHDATE');
    console.log(headers);

    const playerData= player.resultSets[0].rowSet

    playerData.map(i => {const playerInfo = {};
                        playerInfo.firstname = i[playerFirstNameIndex];
                        playerInfo.lastname = i[playerLastNameIndex];
                        playerInfo.birdthdate= i[playerBirthDateIndex]
                        this.playerInfo=playerInfo;
                    }
            );


  });
}
}
