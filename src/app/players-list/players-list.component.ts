import { Component, OnInit } from  '@angular/core';
import { ApiService } from  '../api.service';
import { Observable, of } from 'rxjs';
@Component({
    selector:  'app-player-list',
    templateUrl:  './players-list.component.html',
    styleUrls: ['./players-list.component.css']
})

export  class  PlayersListComponent  implements  OnInit {

public items=[];
constructor(private  apiService:  ApiService) { }
ngOnInit() {
    this.getPlayers();
}
public  getPlayers(){
    this.apiService.getPlayers().subscribe((items: Array<any>) => {
        this.items=items.resultSets;
    });

}
}
